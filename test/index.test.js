const { describe } = require('tape-plus')
const collect = require('collect-stream')
const debug = require('debug')('cobox-group')
const randomWords = require('random-words')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const path = require('path')
const multifeed = require('multifeed')
const Kappa = require('kappa-core')
const { Header } = require('hypertrie/lib/messages')
const memdb = require('level-mem')
const { encodings } = require('cobox-schemas')
const SpaceAbout = encodings.space.about
const crypto = require('cobox-crypto')
const fs = require('fs')

const Log = require('../')

const { replicate, tmp, cleanup } = require('./util')

describe('cobox-log', (context) => {
  context('constructor()', (assert, next) => {
    var storage = tmp()

    var log = Log({
      core: new Kappa(),
      feeds: multifeed(storage),
      db: memdb()
    })

    assert.ok(log.core instanceof Kappa, 'sets this.core')
    assert.ok(log.feeds instanceof multifeed, 'sets this.feeds')
    assert.ok(log.keyPair, 'sets this.keyPair')
    assert.ok(log.db, 'sets this.db')

    cleanup(storage, next)
  })

  context('ready()', async (assert, next) => {
    var storage = tmp()

    var log = Log({
      core: new Kappa(),
      feeds: multifeed(storage),
      db: memdb()
    })

    await log.ready()

    assert.ok(log._feed, 'creates a new hypercore')
    assert.ok(log.get, 'binds hypercore get() to log')
    assert.ok(log.append, 'binds hypercore append() to log')
    assert.ok(log.core.view.log, 'creates a view')
    assert.same(log.read, log.core.view.log.read, 'binds view.read() to read()')

    log.get(0, (err, msg) => {
      var header = Header.decode(msg)

      assert.same(header.type, 'log', 'appends a header')
      cleanup(storage, next)
    })
  })

  context('append()', async (assert, next) => {
    var storage = tmp()

    var log = Log({
      core: new Kappa(),
      feeds: multifeed(storage),
      db: memdb()
    })

    await log.ready()

    var payload = SpaceAbout.encode({
      type: 'space/about',
      timestamp: Date.now(),
      author: crypto.randomBytes(32).toString('hex'),
      content: { name: 'magma' }
    })

    log.append(payload, (err, seq) => {
      assert.error(err, 'no error')
      assert.same(1, seq, 'success')
      cleanup(storage, next)
    })
  })

  context('publish()', async (assert, next) => {
    var storage = tmp()
    var author = crypto.randomBytes(32).toString('hex')

    var log = Log({
      core: new Kappa(),
      feeds: multifeed(storage),
      db: memdb()
    })

    await log.ready()

    var msg = {
      type: 'space/about',
      timestamp: Date.now(),
      author: crypto.randomBytes(32).toString('hex'),
      content: { name: 'magma' }
    }
    var payload = SpaceAbout.encode(msg)
    let data = await log.publish(payload)

    assert.ok(data.key, 'has key')
    assert.same(data.seq, 1, 'seq is 1')
    assert.ok(Buffer.isBuffer(data.value), 'value returns the payload')
    assert.same(msg, SpaceAbout.decode(data.value), 'value is the message')
    cleanup(storage, next)
  })

  context('read()', async (assert, next) => {
    var storage = tmp()

    var log = Log({
      core: new Kappa(),
      feeds: multifeed(storage),
      db: memdb()
    })

    await log.ready()

    var timestamp = Date.now()
    var message = {
      type: 'space/about',
      timestamp,
      author: crypto.randomBytes(32).toString('hex'),
      content: { name: 'magma' }
    }
    var payload = SpaceAbout.encode(message)

    log.append(payload, (err, seq) => {
      assert.error(err, 'no error')
      assert.same(1, seq, 'appends the payload')

      log.get(seq, (err, msg) => {
        assert.error(err, 'no error')
        assert.same(message, SpaceAbout.decode(msg), 'gets the payload')

        var check = [{ key: log._feed.key.toString('hex'), seq: 1, value: message }]
        log.ready(() => {
          collect(log.read({ query: [{ $filter: { value: { type: 'space/about' } } }] }), (err, msgs) => {
            assert.error(err, 'no error')
            assert.same(msgs.length, 1, 'gets one message')
            assert.same(msgs, check, 'message matches')
            cleanup(storage, next)
          })
        })
      })
    })
  })

  context('replication() + read()', (assert, next) => {
    var storage1 = tmp()
    var storage2 = tmp()
    var key = crypto.randomBytes(32)

    var log1 = Log({
      core: new Kappa(),
      feeds: multifeed(storage1, key),
      db: memdb()
    })

    var log2 = Log({
      core: new Kappa(),
      feeds: multifeed(storage2, key),
      db: memdb()
    })

    log1.ready(() => {
      var msg1 = {
        type: 'space/about',
        timestamp: Date.now(),
        author: crypto.randomBytes(32).toString('hex'),
        content: { name: 'blockades' }
      }
      var payload1 = SpaceAbout.encode(msg1)

      var msg2 = {
        type: 'space/about',
        timestamp: Date.now() + 1,
        author: crypto.randomBytes(32).toString('hex'),
        content: { name: 'magma' }
      }
      var payload2 = SpaceAbout.encode(msg2)

      log1.append(payload1, (err, seq) => {
        assert.error(err, 'no error')
        log2.ready(() => {
          replicate(log1.feeds, log2.feeds, (err) => {
            assert.error(err, 'no error')

            var check1 = [{ key: log1._feed.key.toString('hex'), seq: 1, value: msg1 }]
            var check2 = [...check1, { key: log2._feed.key.toString('hex'), seq: 1, value: msg2 }]

            log2.ready(() => {
              collect(log2.read({ query: [{ $filter: { value: { type: 'space/about' } } }] }), (err, msgs1) => {
                assert.error(err, 'no error')
                assert.same(msgs1.length, 1, 'gets one message')
                assert.same(msgs1, check1, 'message matches')

                log2.append(payload2, (err, seq) => {
                  assert.error(err, 'no error')
                  replicate(log1.feeds, log2.feeds, (err) => {
                    assert.error(err, 'no error')

                    log1.ready(() => {
                      collect(log1.read({ query: [{ $filter: { value: { type: 'space/about' } } }] }), (err, msgs2) => {
                        assert.error(err, 'no error')
                        assert.error(err, 'no error')
                        assert.same(msgs2.length, 2, 'gets two messages')
                        assert.same(msgs2, check2, 'messages matches')
                        cleanup([storage1, storage2], next)
                      })
                    })
                  })
                })
              })
            })
          })
        })
      })
    })
  })
})
