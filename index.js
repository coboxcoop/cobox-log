const Indexer = require('kappa-sparse-indexer')
const Query = require('kappa-view-query')
const sub = require('subleveldown')
const Nanoresource = require('nanoresource/emitter')
const { Header } = require('hypertrie/lib/messages')
const { fromMultifeed } = require('kappa-view-query/util')
const fs = require('fs')
const path = require('path')
const maybe = require('call-me-maybe')
const collect = require('collect-stream')
const { EventEmitter } = require('events')
const crypto = require('cobox-crypto')
const hypercoreCrypto = require('hypercore-crypto')
const debug = require('debug')('cobox-log')
const get = require('lodash.get')
const { encodings, validators } = require('cobox-schemas')
const Main = encodings.main

const FEED_NAME = 'log'

const INDEXES = [
  { key: 'log', value: [['value', 'timestamp']] },
  { key: 'clk', value: [['value', 'author'], ['sequence']] },
  { key: 'typ', value: [['value', 'type'], ['value', 'timestamp']] }
]

function tryDecode (schema, value) {
  try { return schema.decode(value) }
  catch (err) { return false }
}

class Log extends Nanoresource {
  /**
   * Create a log handler
   * @constructor
   */
  constructor (opts = {}) {
    super()
    this._id = crypto.randomBytes(2).toString('hex')
    this.core = opts.core
    this.feeds = opts.feeds
    this.keyPair = opts.keyPair || hypercoreCrypto.keyPair()
    this.db = opts.db
    this._validator = this._validator.bind(this)

    this.idx = new Indexer({
      db: sub(this.db, 'idx'),
      name: this._id
    })

    this.view = Query(sub(this.db, 'view'), {
      indexes: INDEXES,
      validator: this._validator,
      getMessage: fromMultifeed(this.feeds, {
        validator: this._validator
      })
    })

    this.core.use(FEED_NAME, this.idx.source(), this.view)
    this.read = this.core.view.log.read

    const onfeed = (feed) => {
      if (this.idx.feed(feed.key)) return
      scopeFeeds(feed, (err) => {
        if (!err) this.idx.add(feed, { scan: true })
      })
    }

    this.feeds.feeds().forEach(onfeed)
    this.feeds.on('feed', onfeed)
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      this.open((err) => {
        if (err) return reject(err)
        this.core.ready(FEED_NAME, (err) => {
          // HACK while we have a race condition
          setTimeout(() => {
            if (err) return reject(err)
            return resolve()
          }, 10)
        })
      })
    }))
  }

  publish(payload = {}, cb) {
    return maybe(cb, new Promise((resolve, reject) => {
      this._feed.append(payload, (err, seq) => {
        if (err) return reject(err)
        return resolve({
          key: this._feed.key.toString('hex'),
          seq,
          value: payload
        })
      })
    }))
  }

  query (query, opts = {}, callback) {
    if (typeof opts === 'function') return this.query(query, {}, opts)
    if (opts.live) return this.read({ query: [query], ...opts })

    return maybe(callback, new Promise((resolve, reject) => {
      collect(this.read({ query: [query] }), (err, msgs) => {
        if (err) return reject(err)
        resolve(msgs)
      })
    }))
  }

  _open (callback) {
    this.feeds.writer('log', {
      keypair: this.keyPair
    }, (err, feed) => {
      if (err) return callback(err)
      this._feed = feed
      this.get = this._feed.get.bind(this._feed)
      this.append = this._feed.append.bind(this._feed)

      if (this._feed.length) return callback()

      this._feed.append(Header.encode({ type: FEED_NAME }), (err, seq) => {
        if (err) return callback(err)
        return callback()
      })
    })
  }

  _validator (msg) {
    // this is somewhat inefficient, we're having to decode
    // once with a simple schema to determine the type, then use
    // that to find our encoder, and decode the rest of the content
    var value = tryDecode(Main, msg.value)
    if (!value) return false
    var type = value.type
    var encoder = get(encodings, type.split('/').join('.'))
    var value = tryDecode(encoder, msg.value)
    if (!value) return false
    msg.value = value
    return msg
  }
}

module.exports = (opts) => new Log(opts)
module.exports.Log = Log

function scopeFeeds (feed, cb) {
  feed.get(0, (err, msg) => {
    if (err) return cb()

    try {
      var header = Header.decode(msg)
      if (header.type !== 'log') return cb(new Error('invalid feed'))
      cb()
    } catch (err) {
      return cb(err)
    }
  })
}
